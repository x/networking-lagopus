===============================
networking-lagopus
===============================

Neutron Driver for Lagopus

OpenStack networking-lagopus is a library of drivers and plugins that integrates
OpenStack Neutron with Lagopus Switch Backend. Lagopus switch is a high-performance
software OpenFlow 1.3 switch.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/networking-lagopus
* Source: http://git.openstack.org/cgit/openstack/networking-lagopus
* Bugs: http://bugs.launchpad.net/networking-lagopus
* Lagopus: http://www.lagopus.org
