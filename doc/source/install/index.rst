=============================================
networking-lagopus service installation guide
=============================================

.. toctree::
   :maxdepth: 2

   install.rst

The networking-lagopus service (networking_lagopus) provides...

This chapter assumes a working setup of OpenStack following the
`OpenStack Installation Tutorial
<https://docs.openstack.org/project-install-guide/ocata/>`_.
